#include <iostream>

using namespace std;

int n, m;
int sol[20], g[25], viz[20];

void citire()
{
    cin>>n>>m;
    for(int i=0; i<n; ++i)
        cin>>g[i];
}

void afis()
{
    for(int i=2; i<m+2; ++i)
        cout<<g[sol[i]]<<" ";
    cout<<'\n';
}

void backt(int k)
{
    if(k == m+2)
    {
        afis();
        return;
    }
    for(int v=0; v<n; ++v)
        if(viz[v] == 0 && g[v] >= g[sol[k-1]] && g[v] > g[sol[k-2]]){
            viz[v] = 1;
            sol[k] = v;
            backt(k+1);
            viz[v] = 0;
    }
}

int main()
{
    citire();
    sol[0] = 24;
    sol[1] = 24;
    g[24] = -1;
    backt(2);
    return 0;
}
