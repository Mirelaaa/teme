#include <fstream>
#include <cstring>
using namespace std;

ifstream f("a.in");
ofstream g("a.out");


int n, n1, sol[30], viz[30], frgen[30];

struct cuvinte{
    char cuv[10];
    int fr[30];
}a[30];

void form_frecv(int i)
{
    int l = strlen(a[i].cuv);
    for(int j=0; j<l; ++j)
        a[i].fr[a[i].cuv[j]-'a']++;
}

void cit_init()
{
    f>>n>>n1;
    f.get();
    for(int i=0; i<n; ++i)
    {
        f>>a[i].cuv;
        form_frecv(i);
    }
}

bool if_add(int index)
{
    int l = strlen(a[index].cuv);
    for(int i=0; i<l; ++i)
    {
        int poz_in_frecv = a[index].cuv[i]-'a';
        if(frgen[poz_in_frecv] + a[index].fr[poz_in_frecv] > n1)
            return 0;
    }
    return 1;
}

void add_frecv(int index, int sign)
{
    int l = strlen(a[index].cuv);
    for(int i=0; i<l; ++i)
    {
        int poz_in_frecv = a[index].cuv[i]-'a';
        frgen[poz_in_frecv] += sign*a[index].fr[poz_in_frecv];
    }
}

void afis(int k)
{
    for(int i=0; i<k; ++i){
        g<<a[sol[i]].cuv;
    }
    g<<'\n';
}


void backt(int k)
{
    if(k != 0)
    {
        afis(k);
    }
    for(int i=0; i<n; ++i)
        if(!viz[i] && if_add(i))
        {
            viz[i] = 1;
            add_frecv(i, 1);
            sol[k] = i;
            backt(k+1);
            add_frecv(i, -1);
            viz[i] = 0;
        }
}


int main()
{
    cit_init();
    backt(0);
    return 0;
}
