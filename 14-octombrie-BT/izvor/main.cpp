///http://campion.edu.ro/arhiva/index.php?page=source&action=view&id=597212
#include <fstream>
using namespace std;

ifstream f("izvor.in");
ofstream g("izvor.out");

int n, p, m, a[25][25], nr;

void cit()
{
    int from, to, type;
    f>>n>>p>>m;
    for(int i=1; i<=p; ++i)
    {
        f>>from>>to>>type;
        a[from][to] = type+1;
        a[to][from] = type+1;
    }
    for(int i=0; i<=n; ++i)
        a[0][i] = a[i][0] = 1;
}
int tip = 0, sol[25], viz[25];
void backt(int k)
{
    if(k == m+1)
    {
        if(tip == 1 && a[sol[k-1]][sol[1]] > 0)
            nr++;
        return;
    }
    for(int c=1; c<=n; c++)
        if(a[sol[k-1]][c] > 0 && viz[c] == 0)
        {
            int tipa = tip;
            if(a[sol[k-1]][c] == 2)
                tip = 1;
            viz[c] = 1;
            a[sol[k-1]][c] *= -1;
            a[c][sol[k-1]] *= -1;
            sol[k] = c;
            backt(k+1);
            viz[c] = 0;
            a[sol[k-1]][c] *= -1;
            a[c][sol[k-1]] *= -1;
            tip = tipa;
        }
}

int main()
{
    cit();
    backt(1);
    g<<nr;
    return 0;
}
