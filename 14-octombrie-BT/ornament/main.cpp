#include <fstream>
using namespace std;

ifstream f("ornament.in");
ofstream g("ornament.out");

int n, cer;

struct patrat{
    int cul[4];
}a[20];

int mat[20][20];
int dx[4]{-1, 0, 1, 0};
int dy[4]{0, 1, 0, -1};

void citire()
{
    f>>n;
    for(int i=1; i<=n; ++i)
        f>>a[i].cul[0]>>a[i].cul[1]>>a[i].cul[3]>>a[i].cul[2];
    f>>cer;
}

void bord()
{
    for(int i=0; i<=n+1; i++)
    {
        mat[i][0] = -1;
        mat[i][n+1] = -1;
        mat[0][i] = -1;
        mat[n+1][i] = -1;
    }
}
int i=1, j=1;

void afis()
{
    for(int i=1; i<=n; i++){
        for(int j=1; j<=n; j++)
            g<<mat[i][j]<<" ";
        g<<'\n';
    }
}



void backt1(int k)
{
    if(k == n+1)
    {
        afis();
        exit(0);
    }
    /**
    N0
    E1
    V2
    S3
    **/
    for(int v=0; v<4; v++)
    {
        int inou = i + dx[v];
        int jnou = j + dy[v];
        if(mat[inou][jnou] == 0)
        {
            int val = a[mat[i][j]].cul[v];
            for(int vi=1; vi <= n; vi++)
                if(a[vi].cul[3-v] == val)
                {
                    i += dx[v];
                    j += dy[v];
                    mat[i][j] = vi;
                    backt1(k+1);
                    i -= dx[v];
                    j -= dy[v];
                }
        }
    }
}

int main()
{
    citire();
    backt1(1);
    return 0;
}
