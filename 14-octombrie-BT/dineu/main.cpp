/// 44
#include <fstream>

using namespace std;

ifstream f("dineu.in");
ofstream g("dineu.out");


struct reprez{
    int nl;
    int cod[15];
}a[25];
int N, L, x;
int ccc[25][25], viz[25];

void strcpy(int a[25], int b[25], int nn)
{
    for(int i=0; i < nn; ++i)
        a[i] = b[i];
}

bool comun(int i, int j)
{
    for(int i1=0; i1 < a[i].nl; ++i1)
        for(int j1=0; j1 < a[j].nl; ++j1)
            if(a[i].cod[i1] == a[j].cod[j1])
                return 1;
    return 0;
}


void cine_cu_cine()
{
    for(int i=0; i<N-1; ++i){
        for(int j=i+1; j<N; ++j){
            if(comun(i, j) == 1)
                ccc[i][j] = ccc[j][i] = 1;
        }
    }
}



void citire()
{
    f>>N>>L;
    for(int i=0; i<N; ++i){
        f>>a[i].nl;
        for(int j=0; j<a[i].nl; ++j){
            f>>a[i].cod[j];
        }
    }
    cine_cu_cine();
}

int vmax, solmax[25];
int sol[25];

bool in_com(int k, int v)
{
    if(k == 0)
        return 1;
    for(int i=0; i<k; ++i)
        if(ccc[i][v] == 0)
            return 0;
    return 1;
}


void backt(int k)
{
    if(k > vmax){
        strcpy(solmax, sol, k);
        vmax =  k;
    }
    if(k >= N)
        return;
    for(int v=k; v < N; ++v)
        if(viz[v] == 0 && in_com(k, v) == 1)
        {
            viz[v] = 1;
            sol[k] = v;
            backt(k+1);
            viz[v] = 0;
        }
}

void afis()
{
    g<<vmax<<'\n';
    for(int i=0; i<vmax; ++i)
        g<<solmax[i]+1<<" ";
}


int main()
{
    ios::sync_with_stdio(false);
    citire();
    backt(0);
    afis();
    return 0;
}
