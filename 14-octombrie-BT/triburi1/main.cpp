#include <fstream>
#include <algorithm>
using namespace std;

ifstream f("triburi1.in");
ofstream g("triburi1.out");

int n, k, p;
int a[30005];
int sol[10];
int poz;


void tobase()
{
    while(poz && p)
    {
        sol[poz] = p%k;
        p = p /k;
        poz--;
    }
}

void cit()
{
    f>>n>>k>>p;
    poz = k-1;
    p--;
    for(int i=1; i<=n; i++)
        f>>a[i];
}


int main()
{
    cit();
    tobase();
    sort(a+1, a+n+1);
    for(int i=1; i<=n; i++)
    {
        if(a[i] != a[i-1])
        {
            for(int j=0; j<k; j++)
                g<<char(sol[j]+'A'+a[i]-1);
            g<<'\n';
        }
    }
    return 0;
}
