#include <fstream>
using namespace std;

ifstream f("banda1.in");
ofstream g("banda1.out");

int n, gmax;
int a[30];
int s[3];

void cit()
{
    f>>n>>gmax;
    for(int i=0; i<n; i++)
        f>>a[i];
}


int nrc=2, vmin=30;

void backt(int k)
{
    if(k == n)
    {
        if(nrc < vmin)
            vmin = nrc;
        return;
    }
    for(int v=0; v<2; v++)
    {
        if(s[v] + a[k] <= gmax)
        {
            s[v] += a[k];
            backt(k+1);
            s[v] -= a[k];
        }
        else{
            int aux = s[v];
            s[v] = a[k];
            nrc++;
            if(nrc < vmin)
                backt(k+1);
            s[v] = aux;
            nrc--;
        }
    }

}

int main()
{
    cit();

    backt(0);

    g<<vmin;

    return 0;
}
