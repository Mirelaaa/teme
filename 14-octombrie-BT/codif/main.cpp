/// http://campion.edu.ro/arhiva/index.php?page=source&action=view&id=597216
#include <fstream>
#include <map>
#include <cstring>
using namespace std;

ifstream f("codif.in");
ofstream g("codif.out");

int n;
char a[100];
char fin[80][100];
char sol[100];
map<int, char> put;
///512
void init()
{
    put[1] = ' ';
    put[2] = 'a';
    put[4] = 'e';
    put[8] = 'i';
    put[16] = 'o';
    put[32] = 'u';
    put[64] = 'm';
    put[128] = 'n';
    put[256] = 'r';
    put[512] = 's';
}

int nr=0;
void backt(int k, int l)
{
    if(k == n)
    {
        strcpy(fin[nr++], sol);
        return;
    }
    int nr=a[k]-'0';
    if(put[nr] != NULL){
        sol[l] = put[nr];
        backt(k+1, l+1);
        sol[l] = '\0';
    }
    if(k + 1 > n)
        return;
    nr = nr*10 + (a[k+1]-'0');
    if(put[nr] != NULL){
        sol[l] = put[nr];
        backt(k+2, l+1);
        sol[l] = '\0';
    }
    if(k + 2 > n)
        return;
    nr = nr*10 + (a[k+2]-'0');
    if(put[nr] != NULL){
        sol[l] = put[nr];
        backt(k+3, l+1);
        sol[l] = '\0';
    }

}

void afis()
{
    g<<nr<<'\n';
    for(int i=0; i<nr-1; i++)
        for(int j=i+1; j<nr; j++)
            if(strlen(fin[i]) > strlen(fin[j]) || (strlen(fin[i]) == strlen(fin[j]) && strcmp(fin[i], fin[j]) > 0))
                swap(fin[i], fin[j]);
    for(int i=0; i<nr; i++)
        g<<fin[i]<<'\n';
}

int main()
{
    ios::sync_with_stdio(false);
    init();
    f>>n;
    f.get();
    f.getline(a, 90);
    init();
    backt(0, 0);
    afis();
    return 0;
}
