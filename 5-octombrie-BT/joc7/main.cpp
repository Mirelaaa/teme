/// http://campion.edu.ro/arhiva/index.php?page=source&action=view&id=596819
#include <cstdio>

using namespace std;
int N, M;
char a[35][35], fr[30];

void citire()
{
    scanf("%d %d\n", &N, &M);
    for(int i=1; i<=N; ++i){
        for(int j=1; j<=M; ++j)
            scanf("%c", &a[i][j]);
        scanf("\n");
    }
    fr[a[1][1]-'A']=1;
}
int i=1, j=1, vmax = 0;

int dx[5]{0, 1, 0, -1};
int dy[5]{1, 0, -1, 0};
void bt(int k)
{
    if(k > vmax)
        vmax = k;
    for(int v=0; v < 5; ++v)
    {
        int inou = i + dx[v];
        int jnou = j + dy[v];
        if(a[inou][jnou] >= 'A' && a[inou][jnou] <= 'Z' && fr[a[inou][jnou]-'A'] == 0)
        {
            i += dx[v];
            j += dy[v];
            fr[a[i][j]-'A'] = 1;
            bt(k+1);
            fr[a[i][j]-'A'] = 0;
            i -= dx[v];
            j -= dy[v];
        }
    }
}


int main()
{
    freopen("joc7.in", "r", stdin);
    freopen("joc7.out", "w", stdout);
    citire();
    bt(1);
    printf("%d", vmax);
    return 0;
}
