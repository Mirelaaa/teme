/// http://campion.edu.ro/arhiva/index.php?page=source&action=view&id=596824
#include <cstdio>
#include <cstdlib>
using namespace std;

int N, K, P, C;
int viz[15], sol[15], dp[15];

int maxi(int i)
{
    int vmax=0;
    for(int j=i+1; j<=N; j++)
        if(sol[j] > sol[i] && dp[j] > vmax)
            vmax = dp[j];
    return vmax;
}


bool verif()
{
    for(int i=1; i<=N; ++i)
        dp[i] = 0;
    dp[N] = 1;
    int vm = 0;
    for(int i=N-1; i>=1; i--)
    {
        dp[i] = 1+maxi(i);
        if(dp[i] > dp[vm])
            vm = i;
    }
    if(dp[vm] >= K)
        return 1;
    return 0;
}

void afis()
{
    for(int i=1; i<=N; ++i)
        printf("%d ", sol[i]);
}


void bt(int k)
{
    if(k == N+1)
    {
        if(verif() == 1)
            C++;
        if(C == P)
            {afis(); exit(0);}

        return;
    }
    for(int i=1; i<=N; i++){
        if(viz[i] == 0)
        {
            sol[k] = i;
            viz[i] = 1;
            bt(k+1);
            viz[i] = 0;
        }
    }
}


int main()
{
    freopen("jucarii.in", "r", stdin);
    freopen("jucarii.out", "w", stdout);
    scanf("%d %d %d", &N, &K, &P);
    bt(1);
    return 0;
}
