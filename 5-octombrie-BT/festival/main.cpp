#include <fstream>
#include <cstring>
#include <map>

using namespace std;

ifstream f("festival.in");
ofstream g("festival.out");

struct trupa{
int m;
char membr[6][25];
};

int n;
trupa t[25];
char a[600];
int impr[30][30];



bool compara(int nr1, int nr2)
{
    for(int i=0; i < t[nr1].m; ++i)
    {
        for(int j=0; j  < t[nr2].m; ++j)
            if(strcmp(t[nr1].membr[i], t[nr2].membr[j]) == 0)
                return 0;
    }
    return 1;
}


void citire()
{
    f>>n;
    f.get();
    for(int i=1; i<=n; ++i)
    {
        f.getline(a, 600);
        char *p = strtok(a, ",");
        while(p != NULL)
        {
            strcpy(t[i].membr[t[i].m++], p);
            p = strtok(NULL, ",");
        }
    }
    for(int i=1; i<=n; ++i)
    {
        impr[i][i] = 1;
        for(int j=i+1; j<=n; j++)
            if(compara(i, j) == 1)
                impr[i][j] = 1;
    }
}
int sol[25], nr = 0, vmax=0;



bool impr_sol(int j, int k)
{
    for(int i=1; i<=k; ++i)
        if(impr[sol[i]][j] == 0)
            return 0;
    return 1;
}

void backt(int k)
{
    if(k-1 > vmax)
        vmax = k-1;
    if(k > n)
        return;
    for(int i=sol[k-1]+1; i <= n; ++i)
        if(impr[k][i] == 1 && impr_sol(i, k-1) == 1)
        {
            sol[k] = i;
            backt(k+1);
            sol[k] = 0;
        }
}



int main()
{
    ios::sync_with_stdio(false);
    citire();
    backt(1);
    g<<vmax;
    return 0;
}
