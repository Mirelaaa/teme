#include <fstream>
#include <cstring>
using namespace std;

ifstream f("concat.in");
ofstream g("concat.out");

int n, vmax, l, pozact=0;
char init[100], lucru[100];
char a[105][100];
int sol[100];
bool potriv(char *s, int pozact)
{
    int lu = strlen(s);
    for(int i=0; i < lu; ++i)
        if(s[i] != init[pozact+i])
            return 0;
    return 1;
}



void bt(int k)
{
    if(strcmp(lucru, init) == 0)
    {
        if(k-1 > vmax)
            vmax = k-1;
        return;
    }
    if(strlen(lucru) > l)
        return;
    for(int i=sol[k-1]+1; i<n; ++i)
        if(potriv(a[i], pozact) == 1)
        {
            sol[k] = i;
            char aux[100];
            strcpy(aux, lucru);
            strcat(lucru, a[i]);
            pozact = strlen(lucru);
            bt(k+1);
            strcpy(lucru, aux);
            pozact = strlen(lucru);
        }
}

void afis()
{
    g<<vmax<<'\n';
    for(int i=1; i<=vmax; ++i)
        g<<sol[i]<<" ";
}



int main()
{
    f.getline(init, 100);
    l = strlen(init);
    f>>n;
    f.get();
    for(int i=1; i<=n; ++i)
        f.getline(a[i], 100);
    bt(1);
    afis();
    return 0;
}
