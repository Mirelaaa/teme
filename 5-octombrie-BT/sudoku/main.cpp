/// http://campion.edu.ro/arhiva/index.php?page=source&action=view&id=596890
#include <cstdio>
#include <cstdlib>
using namespace std;

int s[9][9], nr0;
int viz_l[9][10], viz_c[9][10], viz_p[9][10];
int zeroz[81][2];



void afis()
{
    for(int i=0; i<9; i++)
    {
        for(int j=0; j<9; j++)
            printf("%d", s[i][j]);
        printf("\n");
    }
}

void backt(int k)
{
    if(k == nr0)
    {
        afis();
        exit(0);
    }
    int i = zeroz[k][0], j = zeroz[k][1];
    for(int val=1; val<=9; val++)
        if(viz_l[i][val] == 0 && viz_c[j][val] == 0 && viz_p[(i/3)*3+j/3][val] == 0)
        {
            s[i][j] = val;
            viz_l[i][val] = 1;
            viz_c[j][val] = 1;
            viz_p[(i/3)*3 + j/3][val] = 1;
            backt(k+1);
            viz_l[i][val] = 0;
            viz_c[j][val] = 0;
            viz_p[(i/3)*3 + j/3][val] = 0;
        }
}




int main()
{
    freopen("sudoku.in", "r", stdin);
    freopen("sudoku.out", "w", stdout);
    for(int i=0; i<9; i++)
    {
        for(int j=0; j<9; j++)
        {
            char c;
            scanf("%c", &c);
            s[i][j] = c - '0';
            if(s[i][j] == 0)
            {
                zeroz[nr0][0] = i;
                zeroz[nr0++][1] = j;
            }
            viz_l[i][s[i][j]] = 1;
            viz_c[j][s[i][j]] = 1;
            viz_p[(i/3)*3+j/3][s[i][j]] = 1;
        }
        scanf("\n");
    }
   // afis(viz_l);
  //  afis(viz_c);
  //  afis(viz_p);
  //  printf("\n%d", nr0);
    backt(0);
    return 0;
}
