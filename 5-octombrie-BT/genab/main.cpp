/// http://campion.edu.ro/arhiva/index.php?page=source&action=view&id=596820
#include <cstdio>

using namespace std;

int N;
char abs[2]{'a', 'b'}, sol[25];

void afis()
{
    for(int i=1; i<=N; ++i)
        printf("%c", sol[i]);
    printf("\n");
}


void bt(int k)
{
    if(k == N+1)
    {
        afis();
        return;
    }
    sol[k] = 'a';
    bt(k+1);
    if(sol[k-1] != 'b')
        {sol[k] = 'b'; bt(k+1);}
}


int main()
{
    freopen("genab.in", "r", stdin);
    freopen("genab.out", "w", stdout);
    scanf("%d", &N);
    bt(1);
    return 0;
}
